import React from 'react';
import {Route, Switch} from "react-router-dom";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import Gallery from "./containers/Gallery/Gallery";
import NewPhoto from "./containers/NewPhoto/NewPhoto";

const Routes = () => {
    return (
        <Switch>
            <Route path="/" exact component={Gallery}/>
            <Route path="/photos/:id/:displayName" exact component={Gallery} />
            <Route path="/register" exact component={Register}/>
            <Route path="/login" exact component={Login}/>
            <Route path="/add-photo" exact component={NewPhoto} />
        </Switch>
    );
};

export default Routes;