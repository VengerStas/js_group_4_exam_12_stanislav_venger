import React, {Component} from 'react';
import {Container} from "reactstrap";
import AddPhoto from "../../components/AddPhoto/AddPhoto";
import {photoCreate} from "../../store/actions/photoActions";
import {connect} from "react-redux";

class NewPhoto extends Component {
    createPhoto = photoData => {
        this.props.photoCreate(photoData);
    };

    render() {
        return (
            <div className="new-photo-form">
                <Container>
                    <h4>Add new photo</h4>
                    <AddPhoto
                        onSubmit={this.createPhoto}
                    />
                </Container>
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => ({
    photoCreate: photoData => dispatch(photoCreate(photoData)),
});

export default connect(null, mapDispatchToProps)(NewPhoto);