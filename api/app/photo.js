const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');
const auth = require('../middleware/auth');
const Photo = require('../models/Photo');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', async (req, res) => {
    try {
        const criteria = {};
        if (req.query.user_photo) {
            criteria.user = req.query.user_photo;
        }

        const photos = await Photo.find(criteria).populate('user', '_id, displayName');
        res.send(photos);
    } catch (e) {
        res.sendStatus(500);
    }
});

router.post('/', auth, upload.single('image'), (req, res) => {
    const photoData = {...req.body, user: req.user._id};

    if (req.file) {
        photoData.image = req.file.filename;
    }

    const photo = new Photo (photoData);

    photo.save()
            .then(results => Photo.populate(results, 'user').then(result => res.send(result)))
            .catch(error => res.status(400).send(error));
});

router.delete('/:id', (req, res) => {
    Photo.findByIdAndDelete(req.params.id)
        .then(result => res.send(result))
        .catch(error => res.status(403).send(error))
});


module.exports = router;