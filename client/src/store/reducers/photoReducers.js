import {FETCH_PHOTO_FAILURE, FETCH_PHOTO_SUCCESS} from "../actions/photoActions";

const initialState = {
    photos: [],
    error: null
};


const photoReducers = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_PHOTO_SUCCESS:
            return {...state, photos: action.photos};
        case FETCH_PHOTO_FAILURE:
            return {...state, error: action.error};
        default:
            return state;
    }
};

export default photoReducers;