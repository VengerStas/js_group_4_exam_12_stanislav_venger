import React, {Component, Fragment} from 'react';
import {Alert, Button, Form} from "reactstrap";
import FormElement from "../UI/Form/FormElement";
import {connect} from "react-redux";

class AddPhoto extends Component {
    state = {
        title: '',
        image: null
    };

    submitFormHandler = event => {
        event.preventDefault();

        const formData = new FormData();

        Object.keys(this.state).forEach(key => {
            formData.append(key, this.state[key]);
        });

        this.props.onSubmit(formData);
    };

    inputChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    fileChangeHandler = event => {
        this.setState({
            [event.target.name]: event.target.files[0]
        })
    };

    getFieldError = fieldName => {
        return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message;
    };

    render() {
        return (
            <Fragment>
                {this.props.error && this.props.error.global && (
                    <Alert color="danger">
                        {this.props.error.global}
                    </Alert>
                )}
                <Form onSubmit={this.submitFormHandler}>
                    <FormElement
                        value={this.state.title}
                        onChange={this.inputChangeHandler}
                        type="text" title='Recipe'
                        propertyName='title'
                        placeholder='Enter photo title'
                        error={this.getFieldError('title')}
                        required={true}
                    />

                    <FormElement
                        propertyName="image"
                        title="Image"
                        type="file"
                        onChange={this.fileChangeHandler}
                        error={this.getFieldError('image')}
                        required={true}
                    />
                    <Button type="submit">Add</Button>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    error: state.photos.error
});

export default connect(mapStateToProps, null)(AddPhoto);