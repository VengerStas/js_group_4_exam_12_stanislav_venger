import React, {Component, Fragment} from 'react';
import {
    Button,
    Card,
    CardBody,
    CardTitle,
    Container,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    NavLink
} from "reactstrap";
import {NavLink as RouterNavLink} from "react-router-dom";
import {connect} from "react-redux";

import {deletePhoto, getPhotos} from "../../store/actions/photoActions";
import ImageThumbnail from "../../components/ImageThumbnail/ImageThumbnail";

import './Gallery.css';

class Gallery extends Component {
    state = {
        photoModal: null
    };

    showModal = (id) => {
        this.setState({photoModal: id})
    };

    hideModal = () => {
        this.setState({photoModal: null})
    };

    componentDidMount() {
        this.props.getPhotos(this.props.match.params.id);
    }

    componentDidUpdate(prevProps) {
        if (prevProps.match.params.id !== this.props.match.params.id) {
            this.props.getPhotos(this.props.match.params.id);
        }
    }

    render() {
        let photoCard = null;
        if (this.props.photos) {
            photoCard = this.props.photos.map(photo => {
                return (
                    <Card key={photo._id} className="photo-item">
                        <NavLink onClick={() => this.showModal(photo)}>
                            <ImageThumbnail image={photo.image}/>
                        </NavLink>
                        <CardBody>
                            <CardTitle>{photo.title}</CardTitle>
                            <Button tag={RouterNavLink} className="show-more"  to={`/photos/${photo.user._id}/${photo.user.displayName}`}>By: {photo.user.displayName}</Button>
                            {this.props.user && (this.props.match.params.id === this.props.user._id) ?
                                <Button color="danger" onClick={() => this.props.deletePhoto(photo)}>Delete</Button> : null
                            }

                        </CardBody>
                    </Card>
                )
            });
        }
        return (
            <div className="photo-block">
                <Container>
                    <h4>{this.props.match.params.displayName} Gallery</h4>
                    {
                        this.props.user && (this.props.match.params.id === this.props.user._id) ?
                            <div className="photo-header-block">
                                <NavLink tag={RouterNavLink} to="/add-photo">Add new photo</NavLink>
                            </div> : null
                    }
                    <div className="photo-list">
                        {photoCard}
                    </div>
                    <Modal size="xl" isOpen={!!this.state.photoModal} toggle={this.hideModal}>
                        {
                            this.state.photoModal && (
                                <Fragment>
                                    <ModalHeader toggle={this.hideModal}>{this.state.photoModal.title}</ModalHeader>
                                    <ModalBody>
                                        <ImageThumbnail image={this.state.photoModal.image}/>
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button color="secondary" onClick={this.hideModal}>Close</Button>
                                    </ModalFooter>
                                </Fragment>
                            )
                        }
                    </Modal>
                </Container>
            </div>
        );
    }
}

const mapStateToProps = state => ({
    user: state.users.user,
    photos: state.photos.photos
});

const mapDispatchToProps = dispatch => ({
    getPhotos: userId => dispatch(getPhotos(userId)),
    deletePhoto: photoId => dispatch(deletePhoto(photoId))
});

export default connect(mapStateToProps, mapDispatchToProps)(Gallery);