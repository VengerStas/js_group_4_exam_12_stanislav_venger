const path = require('path');

const rootPath = __dirname;


module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    dbUrl: 'mongodb://localhost/gallery',
    mongoOption: { useNewUrlParser: true, useCreateIndex: true},
    facebook: {
        appId: '442648366290165',
        appSecret: '9c8c8ba7174e0e2669e48a22e584d40b'
    }
};