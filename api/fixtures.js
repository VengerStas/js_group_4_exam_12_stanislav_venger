const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');

const User = require('./models/Users');
const Photo = require('./models/Photo');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOption);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    const users =  await User.create ({
        username: 'user1',
        password: '123',
        displayName: 'User 1',
        token: nanoid()
    }, {
        username: 'user2',
        password: '123',
        displayName: 'User 2',
        token: nanoid()
    });

    await Photo.create(
        {user: users[0]._id, title: 'Обезьяна', image: 'photo1.jpg'},
        {user: users[1]._id, title: 'Природа', image: 'download.jpeg'}
    );

    return connection.close();
};

run().catch(error => {
    console.log('Oops something happened...', error);
});