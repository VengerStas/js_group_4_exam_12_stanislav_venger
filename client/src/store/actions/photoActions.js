import {NotificationManager}  from 'react-notifications';
import axios from '../../axiosBase';
import {push} from "connected-react-router";

export const FETCH_PHOTO_SUCCESS = 'FETCH_PHOTO_SUCCESS';
export const FETCH_PHOTO_FAILURE = 'FETCH_PHOTO_FAILURE';


export const fetchPhotoSuccess = photos => ({type: FETCH_PHOTO_SUCCESS, photos});
export const fetchPhotoFailure = error => ({type: FETCH_PHOTO_FAILURE, error});

export const getPhotos = userId => {
    return dispatch => {
        let path = '/photos';

        if (userId) {
            path += "?user_photo=" + userId;
        }

        return axios.get(path).then(
            response => dispatch(fetchPhotoSuccess(response.data))
        );
    };
};

export const photoCreate = (photoData) => {
    return dispatch => {
        return axios.post('/photos', photoData).then(
            (response) => {
                dispatch(getPhotos(response.data.user._id));
                NotificationManager.info('New photo created!');
                dispatch(push(`/photos/${response.data.user._id}/${response.data.user.displayName}`));
            },
            error => {
                if (error.response) {
                    dispatch(fetchPhotoFailure(error.response.data))
                } else {
                    dispatch(fetchPhotoFailure({Global: 'No connection'}))
                }
            }
        )
    }
};

export const deletePhoto = photo => {
    return (dispatch) => {
        const photoId = photo._id;
        return axios.delete('/photos/' + photoId).then(() => {
            NotificationManager.success('Photo has been deleted');
            dispatch(getPhotos(photo.user._id));
        })
    }
};