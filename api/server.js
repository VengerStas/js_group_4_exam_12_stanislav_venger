const express = require('express');
const mongoose = require ('mongoose');
const config = require('./config');
const cors = require ('cors');

const users = require('./app/user');
const photos = require('./app/photo');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOption).then(() => {
    app.use('/users', users);
    app.use('/photos', photos);
    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});